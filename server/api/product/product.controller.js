'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Product = require('./product.model'),
  util = require('../server.util'),
  _ = require('lodash'),
  productSummaryFields = {
    _id: 1,
    name: 1,
    vision: 1,
    created: 1,
    createdBy: 1,
    updateBy: 1,
    roi: 1
  },
  defaultUserFields = {
    _id: 1,
    username: 1,
    displayName: 1,
    email: 1
  };

/**
 * Show the current
 */
function read(req, res) {
  //console.log(req);
  res.jsonp(req.model);
}

exports.read = read;

exports.delete = function(req, res) {
  var model = req.model;

  model.remove(function(err) {
    if (err) {
      return res.send(400, {
        message: util.getErrorMessage(err)
      });
    } else {
      res.jsonp(model);
    }
  });
};


/**
 * List of Articles
 */
exports.list = function(req, res) {
  var obj = {};

  Product.find(req.query).populate('createdBy', defaultUserFields).populate('updatedBy', defaultUserFields).sort('-created').exec(function(err, products) {
    if (err) {
      return res.send(400, {
        message: util.getErrorMessage(err)
      });
    } else {
      res.jsonp(products);
    }
  });
};

exports.create = function(req, res) {
  var product = new Product(req.body);

  product.save(req, function(err) {
    if (err) {
      util.handleError(err, req);

    } else {
      return getByID(req, res, product._id);
    }
  });
};

/**
 * Update a people
 */
exports.update = function(req, res) {
  var product = req.model;

  product = _.extend(product, req.body);

  product.save(req, function(err) {
    if (err) {
      util.handleError(err, req);
    } else {
      return getByID(req, res, product._id);
    }
  });
};


function getByID(req, res, id) {
    return byID(req, res, function() {
      read(req, res);
    }, id);
  }

function byID(req, res, next, id) {
  console.log("byID called");
  Product.findById(id).populate('personas').populate('createdBy', defaultUserFields).exec(function(err, model) {
    if (err) return next(err);
    if (!model) return next(new Error('Failed to load product ' + id));
    req.model = model;
    if (next) {
      next();
    }
  });
}


exports.byID = byID;
