'use strict';

var express = require('express'),
    products = require('./product.controller'),
    auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', products.list);
router.get('/:productID', products.read);
router.post('/', auth.isAuthenticated(), products.create);
router.put('/:productID', auth.isAuthenticated(), products.update);
router.patch('/:productID', auth.isAuthenticated(), products.update);
router.delete('/:productID', auth.isAuthenticated(), products.delete);

router.param('productID', products.byID);

module.exports = router;
