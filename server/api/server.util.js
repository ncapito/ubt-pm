(function(exports, require) {

  'use strict';

  exports.handleError = handleError;
  exports.getErrorMessage = getErrorMessage;


  var _ = require('lodash');

  function handleError(err, res) {
    console.log(err);
    return res.send(400, {
      message: getErrorMessage(err) || err
    });
  }

  /**
  * Get the error message from error object
  */
  function getErrorMessage(err, prefix) {
    var message = '';

    prefix = prefix || 'Model';

    if (err.code) {
      switch (err.code) {
        case 11000:
          case 11001:
            message = prefix + ' already exists';
            break;
            default:
              message = 'Something went wrong';
            }
          } else {
            for (var errName in err.errors) {
              if (err.errors[errName].message) message = err.errors[errName].message;
            }
          }

          return message;
        }



      })(exports, require);
      
