'use strict';

var express = require('express'),
    personas = require('./persona.controller'),
    auth = require('../../auth/auth.service');

var router = express.Router();

router.param('personaID', personas.byID);
router.get('/', personas.list);
router.get('/:personaID', personas.read);
router.post('/', auth.isAuthenticated(), personas.create);
router.put('/:personaID', auth.isAuthenticated(), personas.update);
router.patch('/:personaID', auth.isAuthenticated(), personas.update);
router.delete('/:personaID', auth.isAuthenticated(), personas.delete);


module.exports = router;
