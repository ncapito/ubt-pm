'use strict';

/**
* Module dependencies.
*/
var mongoose = require('mongoose'),
Schema = mongoose.Schema,
util = require('../model.util');


var PersonaSchema = new Schema({
  createDate: {
    type: Date,
    default: Date.now
  },
  updateDate: {
    type: Date,
    default: Date.now
  },
  createdBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  updatedBy: {
    type: Schema.ObjectId,
    ref: 'User'
  },
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Name cannot be blank'
  },
  description: {
    type: 'String' ,
    default: '',
    trim: true
  },
  mission: {
    type: String,
    default: '',
  },
  behaviors:[ 'OrderedObjectSchema'],
  goals:[ 'OrderedObjectSchema'],
  frustrations: [ 'OrderedObjectSchema'],
  product: {
    type: Schema.ObjectId,
    ref: 'Product'
  }
});



PersonaSchema.methods.getProduct = function(){
  console.log('calling get product');
  if(this.product){
    var Product  = mongoose.model('Product');
    return Product.findById(this.product).exec();

  }
  var promise = new mongoose.Promise();
  console.log('done product is not defined getproduct');
  promise.complete(this);
  return  promise;

};

PersonaSchema.methods.checkDefaults = function(){
  this.frustrations = this.frustrations || [];
  this.behaviors = this.behaviors || [];
  this.features = this.features || [];
};


PersonaSchema.post('init', function(doc) {
  this.checkDefaults();
});

PersonaSchema.pre('save', function(next, req, callback) {
  //figure out how to do this better
  this.checkDefaults();
  util.auditModel(this, req);
  util.checkIds(this.frustrations);
  util.checkIds(this.behaviors);
  util.checkIds(this.goals);
  next(callback);
});


module.exports =  mongoose.model('Persona', PersonaSchema);
