var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var OrderedObjectSchema = new Schema({
  sortOrder: {
    type: Number,
  },
  title: {
    type: String,
    default: '',
    trim: true,
    required: 'Name cannot be blank'
  },
  description: {
    type: String,
    required: 'Name cannot be blank'
  }
});

mongoose.model('OrderedObject', OrderedObjectSchema);
exports.OrderedObject = OrderedObjectSchema;
