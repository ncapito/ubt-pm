(function(angular) {
  'use strict';

  //Articles service used for communicating with the articles REST endpoints
  angular.module('ubtPmApp.product').factory('Product', Product)
    .factory('Persona', Persona);

  /* @ngInject */
  function Product($resource) {
      return $resource('/api/products/:productID', {
        productID: '@_id'
      }, {
        update: {
          method: 'PUT'
        }
      });
    }
    /* @ngInject */
  function Persona($resource) {
    return $resource('/api/personas/:personaID', {
      personaID: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
})(angular);
