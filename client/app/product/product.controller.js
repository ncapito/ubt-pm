(function(angular) {
  'use strict';

  angular.module('ubtPmApp.product').controller('ProductCtrl', ProductCtrl)
    .controller('ProductListCtrl', ProductListCtrl)
    .controller('ProductShowCtrl', ProductShowCtrl);

  /* @ngInject */
  function ProductCtrl(Product, $scope, $state, $location, Auth) {
    $scope.authentication = Auth;
  }

  /* @ngInject */
  function ProductListCtrl(Product, $scope, $log) {
    $scope.randomColor = randomColor;


    var hash = {};

    function pastelColors(){
      var r = (Math.round(Math.random()* 127) + 127).toString(16);
      var g = (Math.round(Math.random()* 127) + 127).toString(16);
      var b = (Math.round(Math.random()* 127) + 127).toString(16);
      return '#' + r + g + b;
    }
    function randomColor(item) {
      if (angular.isUndefined(hash[item])) {
        hash[item] = pastelColors();
      }
      return hash[item];
    }

    Product.query(
      function(data) {
        $scope.products = data;
      },
      function(error) {
        $log.error(error);
      }
    );
  }

  /* @ngInject */
  function ProductShowCtrl(socket, Product, $log, $scope, $state, $anchorScroll, $location) {
    $scope.gotoAnchor = gotoAnchor;
    $scope.$state = $state;
    $scope.loading = false;
    $scope.save = save;
    $scope.isEditing = isEditing;
    $scope.cancel = cancel;
    $scope.edit = edit;
    $scope.prevent = prevent;
    $scope.validate = validate;
    $scope.saveField = saveField;

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('product');
    });

    var _edit = {};


    function saveField(eventType, data) {
      if (validate(eventType, data) === true) {
        save();
      }
    }

    function validate(eventType, data) {
      $log.info($scope.product, data);
      if (angular.isDefined(data) && data) {
        return true;
      }
      return eventType + ' is required';
    }

    function prevent($event) {
      $event.preventDefault();
      $event.stopPropagation();
    }

    function isEditing(key) {
      return _edit[key];
    }

    function cancel(form) {
      if (form) {
        form.$setPristine();
      }
      _edit = {};
      $scope.product.$get();
    }

    function edit(key) {
      _edit[key] = true;
    }

    function save(form) {

      var name = '$update';
      if (!$scope.product._id) {
        name = '$save';
      }

      $scope.product[name](success, error);

      function success() {
        if (form) {
          form.$setPristine();
        }
        $scope.loading = false;
        _edit = {};
        $state.go('product.show', {
          id: $scope.product._id
        });
      }

      function error(result) {
        $log.error(result);
        $scope.loading = false;
      }
    }

    $log.info('loading product ', $state.params.id);
    $scope.product = new Product({
      _id: $state.params.id || undefined
    });
    if ($scope.product._id) {
      $scope.product.$get(function(){
        socket.syncUpdate('product', $scope.product);
      });
    }



    function gotoAnchor(x) {
      if ($location.hash() !== x ) {
        // set the $location.hash to `newHash` and
        // $anchorScroll will automatically scroll to it
        $location.hash(x);
      } else {
        // call $anchorScroll() explicitly,
        // since $location.hash hasn't changed
        $anchorScroll();
      }
    }
  }


})(angular);
