(function(angular) {
  'use strict';
  
  angular.module('ubtPmApp.product', []);
  angular.module('ubtPmApp.product').config(ProductStateConfig);

	/* @ngInject */
  function ProductStateConfig($stateProvider) {
    $stateProvider.
    state('product', {
      url: '/product',
      abstract: true,
      template: '<div ui-view> </div>',
      controller: 'ProductCtrl'
    }).
    state('product.list', {
      url: '',
      templateUrl: 'app/product/product.list.html',
      controller: 'ProductListCtrl'
    }).
    state('product.show', {
        url: '/:id',
        templateUrl: 'app/product/product.show.html',
        controller: 'ProductShowCtrl'
      })
      .state('product.show.newpersona', {
        url: '/persona',
        templateUrl: 'app/product/persona/persona.new.html',
        controller: 'PersonaShowCtrl'
      })
      .state('product.show.persona', {
        url: '/persona/:personaID',
        templateUrl: 'app/product/persona/persona.show.html',
        controller: 'PersonaShowCtrl'
      });
  }

})(angular);
