'use strict';

angular.module('ubtPmApp')
.controller('AppCtrl', AppCtrl);

function AppCtrl($scope, $http, socket, Auth, $timeout) {
  $scope.Auth = Auth;
  $scope.alerts = [];

  socket.socket.on('user:connected', function (item) {
    $timeout(function(){
      $scope.alerts.push({type:'success', msg: item.name + ' connected'});
    });
  });
  socket.socket.on('user:disconnected', function (item) {
    $timeout(function(){
      $scope.alerts.push({type:'success', msg: item.name + ' disconnected'});
    });
  });

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };
  
  $scope.$on('$destroy', function () {
    socket.socket.removeAllListeners('user:connected');
  });
}
