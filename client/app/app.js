(function(angular) {
  'use strict';

  window.String.prototype.capitalize = function(){ return this.charAt(0).toUpperCase() + this.slice(1); };
  window.String.prototype.uncapitalize = function(){
    return (this.substring(0, 1) || '').toLowerCase() + (this.substring(1) || '');
  };

  angular.module('ubtPmApp', [
      'ngCookies',
      'ngAnimate',
      'ngResource',
      'ngSanitize',
      'btford.socket-io',
      'ui.router',
      'ui.bootstrap',
      'ubtPmApp.product',
      'ui.utils',
      'angular-loading-bar',
      'angular-spinkit',
      'xeditable'
    ]).config(Config)
    .factory('authInterceptor', AuthInterceptor)
    .run(Run);

  function Config($stateProvider, $urlRouterProvider, $locationProvider,
    $httpProvider) {

    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
  }

  function AuthInterceptor($rootScope, $q, $cookieStore, $location) {
    return {
      // Add authorization token to headers
      request: function(config) {
        config.headers = config.headers || {};
        if ($cookieStore.get('token')) {
          config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
        }
        return config;
      },

      // Intercept 401s and redirect you to login
      responseError: function(response) {
        if (response.status === 401) {
          $location.path('/login');
          // remove any stale tokens
          $cookieStore.remove('token');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }

  function Run($rootScope, $location, Auth, editableOptions, editableThemes) {
    // Redirect to login if route requires auth and you're not logged in
    editableOptions.theme = 'bs3';
    editableThemes.bs3.formTpl = '<form class="form" role="form"></form>';

    $rootScope.$on('$stateChangeStart', function(event, next) {
      Auth.isLoggedInAsync(function(loggedIn) {
        if (next.authenticate && !loggedIn) {
          $location.path('/login');
        }
      });
    });
  }

})(angular);
